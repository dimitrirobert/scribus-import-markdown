/*
For general Scribus (>=1.3.2) copyright and licensing information please refer
to the COPYING file provided with the program. Following this notice may exist
a copyright and/or license notice that predates the release of Scribus 1.3.2
for which a new license (GPL+exception) is in place.
*/
#include <QObject>
#include <QByteArray>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QStringList>
#include <QTextCodec> 

#include "markdown.h"
#include "scribusstructs.h"
#include "gtframestyle.h"
#include "gtparagraphstyle.h"
#include "util.h"

QString FileFormatName()
{
	return QObject::tr("Markdown Files");
}

QStringList FileExtensions()
{
	return QStringList("md");
}

void GetText(const QString& filename, const QString& encoding, bool textOnly, gtWriter *writer)
{
	Markdown* mdim = new Markdown(filename, encoding, textOnly, writer);
	delete mdim;
}

Markdown::Markdown(const QString& fname, const QString& enc, bool textO, gtWriter *w)
{
	filename = fname;
	encoding = enc;
	writer = w;
	textOnly = textO;
	loadText();
	write();
}

void Markdown::loadText()
{
	QByteArray rawText;
	if (loadRawText(filename, rawText))
		text = toUnicode(rawText);
}

void Markdown::write()
{
	QMap<QString, gtParagraphStyle*> pstyles;
	gtFrameStyle *fstyle = writer->getDefaultStyle();
	QStringList list = text.split("\n", QString::KeepEmptyParts);
        gtParagraphStyle *useStyle = nullptr;
        
        QString paragraph;
        QString appendText;
        QString styleName;
        MdParagraph pstyle;
        int level;
        QString liChar;
        
	for (int i = 0; i < static_cast<int>(list.size()); ++i)
	{
		QString tmpText(list[i]);
		QString tmpText2(tmpText);
		tmpText2=tmpText2.simplified();
                pstyle = DEFAULT;
                /*
		int numberOfWords = tmpText2.count(" ");
		++numberOfWords; */
		useStyle = nullptr;
                appendText = "";
                
                if (tmpText2.startsWith("#")) pstyle = HEADING;
                
                if (tmpText2.contains(QRegularExpression("^[*+-] "))) pstyle = ULIST;
                
                if (tmpText2.contains(QRegularExpression("^\\d+\\. "))) pstyle = OLIST;
                
                if (tmpText2.size() == 0) pstyle = EMPTY;
                
                switch (pstyle)
                {
                    case HEADING:
                        level = tmpText.indexOf(" ");
                        appendText = tmpText.remove(0, level + 1);
                        styleName = QString("Titre%1").arg(level);
                        break;
                    case ULIST:
                        level = (tmpText.indexOf(QRegularExpression("[*+-] ")) / 4) + 1;
                        appendText = tmpText2.remove(0, 2);
                        styleName = QString("Bullet list %1").arg(level);
                        break;
                    case OLIST:
                        level = (tmpText.indexOf(QRegularExpression("\\d+\\. ")) / 4) + 1;
                        appendText = tmpText2.remove(0, 3);
                        styleName = QString("Ordered list %1").arg(level);
                        break;
                    case BLOCKQUOTE:
                        break;
                    case CODE:
                        break;
                    case EMPTY:
                        appendText = paragraph;
                        paragraph = "";
                        styleName = "Courant";
                        break;
                    case DEFAULT:
                        if (paragraph != "") paragraph.append(" ");
                        paragraph.append(tmpText2);
                        break;
                }
                
                pstyles[styleName] = new gtParagraphStyle(*fstyle);
                useStyle = pstyles[styleName];
                useStyle->setName(styleName);
                
                // appendText.append(QString(" %1").arg(pstyle));
                
                if (appendText != "")
                {
                    if (i == static_cast<int>(list.size()) - 1)
			writer->append(appendText, useStyle);
                    else
			writer->append(appendText + "\n", useStyle);
                }
	}
	
}

QString Markdown::toUnicode(const QByteArray& rawText)
{
	QTextCodec *codec;
	if (encoding.isEmpty())
		codec = QTextCodec::codecForLocale();
	else
		codec = QTextCodec::codecForName(encoding.toLocal8Bit());
	QString unistr = codec->toUnicode(rawText);
	return unistr;
}

Markdown::~Markdown()
{

}

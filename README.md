# Import Markdown pour Scribus

Plugin d'import de document au format Markdown pour Scribus, logiciel libre de publication assistée par ordinateur.

## Fonctionnalités

Écriture d'un plugin simple pour importer du Markdown dans Scribus et créer les styles nécessaires.

Gestion des styles de paragraphes :

* titres (tous niveaux)
* listes à puces (plusieurs niveaux d'imbrication)
* listes nuémrotées (plusieurs niveaux d'imbrication)
* paragraphes simples

À venir :

* blocs de citation (blockquote)
* code
* imbrication de blocs différents
* styles de caractères : gras, italique, gras + italique, url
* import des images en tant que cadres d'image
* mise en forme initiale si les styles n'existent pas déjà
* nommage des styles selon la langue

## Compilation

Copiez les fichiers dans les sources de Scribus dans le dossier `scribus/plugins/gettext/markdown`.

Compilez Scribus.

Vous pouvez alors importer un fichier `.md` dans un cadre de texte.
